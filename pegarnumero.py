import pyautogui
import time

def click_image(image_path, confidence=0.7):
    img = pyautogui.locateCenterOnScreen(image_path, confidence=confidence)
    if img is not None:
        pyautogui.click(img.x, img.y)
        print(f"Imagem '{image_path}' encontrada e clicada!")
        return True
    else:
        print(f"Imagem '{image_path}' não encontrada.")
        return False

def press_key_for_duration(key, duration):
    pyautogui.keyDown(key)
    time.sleep(duration)
    pyautogui.keyUp(key)

def press_combination_for_duration(keys, duration):
    for key in keys:
        pyautogui.keyDown(key)
    time.sleep(duration)
    for key in keys:
        pyautogui.keyUp(key)

def main():
    image_path = 'image copy.png'
    vitoria = 'vitoria.png'
    testebot = 'testebot.png'
    conti = 'continue.png'
    play = 'play.png'
    while True:
        if click_image(testebot):
            # Pressiona 'ctrl' + 'r' por 0.1 segundos
            press_combination_for_duration(['ctrl', 'r'], 0.1)
            time.sleep(1)  # Espera por 1 segundo após a ação
        # if click_image(image_path):
        #     # Pressiona 'z' por 5 segundos
        #     press_key_for_duration('z', 5)
        #     time.sleep(1)  # Espera por 1 segundo após a ação
        if click_image(vitoria):
            # Pressiona 'z' por 0.1 segundos
            time.sleep(1)  # Espera por 1 segundo após a ação
        if click_image(conti):
            # Pressiona 'z' por 0.1 segundos
            time.sleep(1)  # Espera por 1 segundo após a ação
        if click_image(play):
            # Pressiona 'z' por 0.1 segundos
            press_key_for_duration('z', 0.1)
            time.sleep(1)  # Espera por 1 segundo após a ação
        else:
            # Se a imagem não foi encontrada, continua movendo para a esquerda e para a direita
            pyautogui.keyDown('right')
            time.sleep(0.5)
            pyautogui.keyUp('right')

            pyautogui.keyDown('left')
            time.sleep(0.5)
            pyautogui.keyUp('left')

if __name__ == "__main__":
    main()
