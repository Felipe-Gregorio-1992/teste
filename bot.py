import pyautogui
import pytesseract
from PIL import Image
import re
import time
from PIL import Image, ImageDraw, ImageFont


# Configuração do caminho do executável do Tesseract
pytesseract.pytesseract.tesseract_cmd = r'C:\Users\felip\Documents\bot\tesseract.exe'

# Esperar um pouco para garantir que a tela do jogo esteja pronta
time.sleep(2)

# Capturar a tela do jogo
imagem = pyautogui.screenshot(region=(400, 530, 1200, 500))

# Usar pytesseract para extrair texto
texto_extraido = pytesseract.image_to_string(imagem)

# Utilizar expressão regular para extrair os números
numeros = re.findall(r'\d+', texto_extraido)

if len(numeros) >= 2:
    num1 = int(numeros[0])
    num2 = int(numeros[1])

    # Calcular o resultado da soma
    resultado = num1 + num2

    # Imprimir o resultado






# Gerar um número aleatório
numero_aleatorio = resultado

def create_image():
    # Tamanho da imagem
    width, height = 70, 50

    # Criar uma nova imagem RGB
    image = Image.new('RGB', (width, height), color=(-255, -255, -255))

    # Obter um objeto de desenho
    draw = ImageDraw.Draw(image)

    # Escolher uma fonte e tamanho maior
    font_size = 40  # Tamanho da fonte aumentado
    font = ImageFont.truetype("calibri.ttf", font_size)

    # Escrever o número aleatório no centro da imagem
    text = f"{numero_aleatorio}"
    text_width, text_height = draw.textsize(text, font=font)
    text_x = (width - text_width) / 2
    text_y = (height - text_height) / 2
    draw.text((text_x, text_y), text, fill=(255,255,255), font=font)

    # Salvar a imagem como PNG
    image.save('numerox.png')

create_image()


img = pyautogui.locateCenterOnScreen('numerox.png')

if img is not None:
    pyautogui.click(img.x, img.y)
else:
    print("Imagem não encontrada.")